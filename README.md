Known bugs <br>
- Errors have not yet been forwarded to the
error tab; they output in the console for the 
time being though
- Mod/delete buttons do not have full functionality
yet
- Listener for the completion tab not yet 
implemented

<br>

Bibliiography of sources consulted <br>
- "How to Write Doc Comments for the Javadoc Tool." N.p, n.d. Web. 22 Oct. 2020 <https://www.oracle.com/technical-resources/articles/java/javadoc-tool.html>.
- Thompson, Errol. TemperatureRecording. 2020. https://gitlab.com/FoOOSD/temperaturerecording
- "SWING - JCheckBox Class - Tutorialspoint." N.p, n.d. Web. 22 Oct. 2020 <https://www.tutorialspoint.com/swing/swing_jcheckbox.htm>.
- "Package javax.swing. Documentation" N.p, n.d. Web. 22 Oct. 2020 <https://docs.oracle.com/javase/7/docs/api/javax/swing/package-summary.html>.